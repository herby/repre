# repre
Recurring (looping until succeeded) promise.

## API

Get the class with `var Repre = require('repre')`.

 - `var repre = new Repre(factoryFn, step = 100)`

Creates new recurring promise. You must pass
the factory function to the constructor.
This factory is used when looping.

 - `repre.loop()`

The factory function will create the promise
that it is tried to resolve; in case of rejection,
another try is done after `step` ms, over and over.

 - `repre.stop()`

Signals the actually looping promise that it should not
start another iteration in case of rejection; and returns it.
This signals that looping should stop, and the original promise
(as well as `.stop()` which just returns it) evetually either
resolve or reject.

Calling `stop()` for already noniterating (resolved
or stopped before) Repre is safe and immediately resolves.

 - `repre.stopAndForget()`

Returns `repre.stop().catch(noop)`, that is,
`stopAndForget` returns a promise which is
resolved whenever running promise is settled,
no matter if by resolving or by rejection.

## Uses

Useful in functional tests, for example using WebDriver.
One sets up a scenario and then returns a Repre which
tries to read all things that eventually appear in a page
up to final assertions; plus `setTimeout(() => repre.stop(), someTimeBeforeTestTimesOut)`.
That way, things are tried until either resolved,
or they are rejected (with correct error) just before test would time out.

It is also good to call `repre.stopAndForget().then(...)` in `afterEach`.
